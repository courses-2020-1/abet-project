# & .\run.ps1
Clear-Host
Write-Host "Executing"
pushd .\src\TeX\templates
# Remove-Item -LiteralPath "$pwd\.venv" -Force -Recurse
# conda env create -f .\environment.yml --prefix .venv
conda activate .\.venv
# https://stackoverflow.com/a/37663645/9302545
$env:Path="C:\Program Files\Inkscape\bin;"+$env:Path
arara biblatex-apa biblatex-apa-test dune-user-guide-es guideofuser template
popd
