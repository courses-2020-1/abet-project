# [ABET](https://www.abet.org) project group N$`^{\circ}11`$

In this [repository](https://gitlab.com/courses-2020-1/abet-project)
[we](https://gitlab.com/courses-2020-1/abet-project#members) are following this
[guide](https://www.scivision.dev/overleaf-with-github) for use between
[Overleaf](https://www.overleaf.com) and [GitLab](https://gitlab.com).

## Members

This work is composed of the following undergraduate students of Mathematics and Computer Science:

<table>
  <tbody>
    <tr>
      <td align="center">
        <a href="https://gitlab.com/carlosal1015">
          <img width="150" src="https://assets.gitlab-static.net/uploads/-/system/user/avatar/2012757/avatar.png?width=90">
          </br>
          <a href="mailto:caznaranl@uni.pe?cc=btorresa@uni.pe"> carlosal1015</a>
        </a>
      </td>
			<td align="center">
        <a href="https://gitlab.com/avalerom">
          <img width="150" src="https://secure.gravatar.com/avatar/07177deabce49598410715ff65aadbe7?s=180&d=identicon">
          </br>
          <a href="mailto:avalerom@uni.pe?cc=btorresa@uni.pe"> avalerom</a>
        </a>
      </td>
			<td align="center">
        <a href="https://gitlab.com/OscarA17">
          <img width="150" src="https://secure.gravatar.com/avatar/800d64d8e77617a79b2eb90dccce2f89?s=180&d=identicon">
          </br>
          <a href="mailto:oscar.arca.a@uni.pe?cc=btorresa@uni.pe">OscarA17</a>
        </a>
      </td>
			<td align="center">
        <a href="ttps://gitlab.com/">
          <img width="150" src="https://secure.gravatar.com/avatar/980003c1e3fead96d73234adb4a17b32?s=180&d=identicon">
          </br>
          <a href="mailto:rccohuanquia@uni.pe?cc=btorresa@uni.pe">RCA9419</a>
        </a>
      </td>
			<td align="center">
        <a href="https://gitlab.com/btorresa">
          <img width="150" src="https://secure.gravatar.com/avatar/40fc868826e786def7ac81f6a99ec055?s=180&d=identicon">
          </br>
          <a href="mailto:btorresa@uni.pe"> btorresa</br>(<b> Delegate</b>)</a>
        </a>
      </td>
    </tr>
  <tbody>
</table>

## Builds :factory:

### To download :incoming_envelope:

<ul>
    <li>
        <a href="https://gitlab.com/courses-2020-1/abet-project/-/jobs/artifacts/master/raw/src/TeX/article/main.pdf?job=build_pdf">
        <code>main.pdf</code></a>
    </li>
    <li>
        <a href="https://gitlab.com/courses-2020-1/abet-project/-/jobs/artifacts/master/raw/src/TeX/beamer/beamer.pdf?job=build_pdf"> <code>beamer.pdf</code></a>
    </li>
        <li>
        <a href="https://gitlab.com/courses-2020-1/abet-project/-/jobs/artifacts/master/raw/src/TeX/beamer/dark.pdf?job=build_pdf">
        <code>dark.pdf</code></a>
    </li>
    <li>
        <a href="https://gitlab.com/courses-2020-1/abet-project/-/jobs/artifacts/master/raw/src/TeX/banner/banner.pdf?job=build_pdf"> <code>banner.pdf</code></a>
    </li>
    <li>
        <a href="https://gitlab.com/courses-2020-1/abet-project/-/jobs/artifacts/master/raw/src/TeX/templates/biblatex-apa.pdf?job=build_pdf"> <code>biblatex-apa.pdf</code></a>
    </li>
    <li>
        <a href="https://gitlab.com/courses-2020-1/abet-project/-/jobs/artifacts/master/raw/src/TeX/templates/biblatex-apa-test.pdf?job=build_pdf"><code>biblatex-apa-test.pdf</code></a>
    </li>
    <li>
        <a href="https://gitlab.com/courses-2020-1/abet-project/-/jobs/artifacts/master/raw/src/TeX/templates/template.pdf?job=build_pdf"> <code>template.pdf</code></a>
    </li>
    <li>
        <a href="https://gitlab.com/courses-2020-1/abet-project/-/jobs/artifacts/master/raw/src/TeX/templates/guideofuser.pdf?job=build_pdf"> <code>guideofuser.pdf</code></a>
    </li>
    <li>
        <a href="https://gitlab.com/courses-2020-1/abet-project/-/jobs/artifacts/master/raw/public/my-vignette.html?job=pages"> <code>my-vignette.html</code></a>
    </li>
    <li>
        <a href="https://gitlab.com/courses-2020-1/abet-project/-/jobs/artifacts/master/raw/public/renv.html?job=pages"><code>renv.html</code></a>
    </li>
    <li>
        <a href="https://gitlab.com/courses-2020-1/abet-project/-/jobs/artifacts/master/raw/public/compiledCode-knitr.pdf?job=pages"> <code>compiledCode-knitr.pdf</code></a>
    </li>
    <li>
        <a href="https://gitlab.com/courses-2020-1/abet-project/-/jobs/artifacts/master/raw/public/v84i08.pdf?job=pages"><code>v84i08.pdf</code></a>
    </li>
</ul>

### View document in web browser :globe_with_meridians:

<ul>
    <li>
        <a href="https://courses-2020-1.gitlab.io/abet-project/main.pdf"><code>main.pdf</code></a>
    </li>
    <li>
        <a href="https://courses-2020-1.gitlab.io/abet-project/beamer.pdf"><code>beamer.pdf</code></a>
    </li>
    <li>
        <a href="https://courses-2020-1.gitlab.io/abet-project/dark.pdf"><code>dark.pdf</code></a>
    </li>
    <li>
        <a href="https://courses-2020-1.gitlab.io/abet-project/banner.pdf"><code>banner.pdf</code></a>
    </li>
    <li>
        <a href="https://courses-2020-1.gitlab.io/abet-project/biblatex-apa.pdf"><code>biblatex-apa.pdf</code></a>
    </li>
    <li>
        <a href="https://courses-2020-1.gitlab.io/abet-project/biblatex-apa-test.pdf"><code>biblatex-apa-test.pdf</code></a>
    </li>
    <li>
        <a href="https://courses-2020-1.gitlab.io/abet-project/template.pdf"><code>template.pdf</code></a>
    </li>
    <li>
        <a href="https://courses-2020-1.gitlab.io/abet-project/guideofuser.pdf"><code>guideofuser.pdf</code></a>
    </li>
    <li>
        <a href="https://courses-2020-1.gitlab.io/abet-project/introduction.html"><code>introduction.html</code></a>
    </li>
    <li>
        <a href="https://courses-2020-1.gitlab.io/abet-project/renv.html"><code>renv.html</code></a>
    </li>
    <li>
        <a href="https://courses-2020-1.gitlab.io/abet-project/compiledCode-knitr.pdf"><code>compiledCode-knitr.pdf</code></a>
    </li>
    <li>
        <a href="https://courses-2020-1.gitlab.io/abet-project/v84i08.pdf"><code>v84i08.pdf</code></a>
    </li>
</ul>

This is the [Overleaf's project mirror (deprecated)](https://www.overleaf.com/read/djyngkxtcbdf).

[:books: References](https://drive.google.com/drive/folders/16aD0bh7oCXOokusK_HAeYBv0SsSa1En5?usp=sharing)

## Clone

```console
[carlosal1015@arch ]$ git clone --depth=1 git@gitlab.com:courses-2020-1/abet-project.git
[carlosal1015@arch ]$ cd abet-project
[carlosal1015@arch ]$ git submodule update --init --remote --recursive --merge
```
