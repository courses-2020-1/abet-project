## Git commands

```console
~/abet project » git push
~/abet project » git push overleaf master
~/abet project » git push origin master
~/abet project » git pull overleaf master
~/abet project » git pull origin master
```

[Download git for windows](https://git-scm.com/download/win)
[Figma](https://help.figma.com/hc/en-us/articles/360041061214-Verify-your-Education-status)


## [`gitlab-runner`](https://docs.gitlab.com/runner)

```console
~/abet project » gitlab-runner register
~/abet project » gitlab-runner list
~/abet project » gitlab-runner exec shell plot_r
~/abet project » gitlab-runner exec docker test_python
```

To update the submodule to the latest remote commit.

```console
git submodule update --remote --merge
```
