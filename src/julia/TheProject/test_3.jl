#!/usr/bin/env julia

if isfile("Project.toml") && isfile("Manifest.toml")
    using Pkg
    Pkg.activate("")
end

using PyPlot
PyPlot.svg(true)
pygui(false)

x = range(0; stop=2*pi, length=1000); y = sin.(3 * x + 4 * cos.(2 * x));
plot(x, y, color="red", linewidth=2.0, linestyle="--");
title("A sinusoidally modulated sinusoid");
savefig("plot.svg");
