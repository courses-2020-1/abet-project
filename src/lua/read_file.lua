#!/usr/bin/lua

-- Adapted from:
-- https://stackoverflow.com/q/11201262/9302545
-- https://stackoverflow.com/q/41942289/9302545
-- http://lua-users.org/wiki/SplitJoin
-- https://gitlab.com/islandoftex/checkcites/-/blob/master/checkcites.lua
local open = io.open

function file_exists(path)
    local f = open(path, "rb") -- r read mode and b binary mode
    if f then f:close() end
    return f ~= nil
end

function read_file(path)
    local file = open(path, "rb")
    if not file then return nil end
    local content = file:read "*a" -- *a or *all reads the whole file
    file:close()
    return content
end

create_table_from_file = function(path)
    local file = open(path, "rb")
    if not file then return nil end

    local lines = {}

    for line in io.lines(path) do
        local words = {}
        for word in line:gmatch("%w+") do table.insert(words, word) end
        table.insert(lines, words)
    end

    file:close()
    return lines
end

function mysplit(inputstr, sep)
    if sep == nil then sep = "%s" end
    local t = {}
    for str in string.gmatch(inputstr, "([^" .. sep .. "]+)") do
        table.insert(t, str)
    end
    return t
end

function string2array(path)
    local t = {}
    s = read_file(path)
    for k, v in string.gmatch(s, "(%w+)=(%w+)") do t[k] = v end
    return t
end
