#!/usr/bin/lua

require('read_file')

local main = '../TeX/article/output.txt'
local PATH = 'sample.txt'

print("Using " .. _VERSION .. " " .. os.date("%Y/%m/%d %H:%M:%S hrs."))

print(PATH .. "'s content:\n" .. read_file(PATH))
print(PATH .. "'s content:\n" .. read_file_by_lines(PATH))

print(main .. "'s content:\n" .. read_file(main))
print(main .. "'s content:\n" .. read_file_by_lines(main))

print("There exists the file " .. PATH .. " in the working directory? " ..
          tostring(file_exists(PATH)):gsub("^%l", string.upper))
HALLO = "hallo.txt"
print("There exists the file " .. HALLO .. " in the working directory? " ..
          tostring(file_exists(HALLO)):gsub("^%l", string.upper))
