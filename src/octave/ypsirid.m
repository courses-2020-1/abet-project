function ypsirid = ypsirid(t,y)
  global olda oldb
  ypsirid(1) = -olda*y(1)*y(2);
  ypsirid(2) = olda*y(1)*y(2) - oldb*y(2);
  ypsirid(3) = oldb*y(2);
  ypsirid = [ypsirid(1) ypsirid(2) ypsirid(3)]';
end
