clear all;
t0 = 0;
tf = 50;
y0 = [99 1 0];
[t y] = ode45('ypsir', [t0 tf], y0);
plot(t, y(:,1), t, y(:,2), t, y(:,3));
title("Name");
xlabel("time");
ylabel("susceptible, infected, recovered");
print -depsc figure1.eps
%saveas (1, "figure1.eps");
