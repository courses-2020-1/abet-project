# Copyleft (c) August, 2020, Oromion.

FROM mtmiller/octave:5

LABEL maintainer="Oromion <caznaranl@uni.pe>" \
	name="Octave" \
	description="Octave plus git." \
	url="https://gitlab.com/courses-2020-1/abet-project/container_registry" \
	vcs-url="https://gitlab.com/courses-2020-1/abet-project" \
	vendor="Oromion Aznarán" \
	version="1.0"

ENV MAIN_PKGS="\
	software-properties-common tzdata" \
	TZ="America/Lima" \
	DEBIAN_FRONTEND=noninteractive

RUN apt-get update -qq && \
	apt-get install -yq --no-install-recommends $MAIN_PKGS && \
	apt-add-repository ppa:git-core/ppa && \
	apt-get install -yq --no-install-recommends git && \
    rm -rf /var/lib/apt/lists/*
