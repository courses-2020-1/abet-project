# Copyleft (c) August, 2020, Oromion.
# Docs: https://pctex.com/kb/74.html
# apt-get vs apt: https://askubuntu.com/a/990838/791670
# curl -sfL https://raw.githubusercontent.com/carlosal1015/mathtime-installer/carlosal1015/mtpro2-texlive.sh | sh +m -i mtp2lite.zip.tpm

FROM registry.gitlab.com/islandoftex/images/texlive

LABEL maintainer="Oromion <caznaranl@uni.pe>" \
    name="Mtpro2 TeXLive" \
    description="Mtpro2 in TeXLive." \
    url="https://gitlab.com/courses-2020-1/abet-project/container_registry" \
    vcs-url="https://gitlab.com/courses-2020-1/abet-project" \
    vendor="Oromion Aznarán" \
    version="1.0"

ENV MAIN_PKGS="\
    git ghostscript inkscape" \
    PIP_PKGS="\
    python3-requests python3-bs4" \
    DEBIAN_FRONTEND=noninteractive \
    JULIA_PATH=/usr/local/julia \
    PATH=$JULIA_PATH/bin:$PATH \
    # JULIA_GPG=3673DF529D9049477F76B37566E3C7DC03D6E495 \
    JULIA_VERSION=1.10.4

RUN rm -f /etc/localtime && \
    ln -s /usr/share/zoneinfo/America/Lima /etc/localtime && \
    apt-get update -qq && \
    apt-get install -yq --no-install-recommends $MAIN_PKGS && \
    rm -rf /var/lib/apt/lists/* && \
    cd /tmp && \
    wget -q https://julialang-s3.julialang.org/bin/linux/x64/1.10/julia-1.10.4-linux-x86_64.tar.gz && \
    tar -xzf julia-1.10.4-linux-x86_64.tar.gz -C "$JULIA_PATH" --strip-components 1 && \
    rm julia-1.10.4-linux-x86_64.tar.gz && \
    wget -q https://raw.githubusercontent.com/carlosal1015/mathtime-installer/carlosal1015/mtp2lite.zip.tpm && \
    unzip -qq mtp2lite.zip.tpm && \
    cp -R texmf/* $(kpsewhich --var-value TEXMFLOCAL) && \
    mkdir -p ~/.local/share/fonts && \
    wget -q https://github.com/tonsky/FiraCode/archive/6.2.zip && \
    unzip -jq 6.2.zip 'FiraCode-6.2/distr/ttf/*.ttf' -d ~/.local/share/fonts && \
    wget -q https://github.com/alexeiva/yanone-kaffeesatz/archive/2.000.zip && \
    unzip -jq 2.000.zip 'yanone-kaffeesatz-2.000/fonts/ttf/*.ttf' -d ~/.local/share/fonts && \
    wget -q https://software.sil.org/downloads/r/andika/Andika-5.000.zip && \
    unzip -jq Andika-5.000.zip 'Andika-5.000/*.ttf' -d ~/.local/share/fonts && \
    fc-cache -f && \
    rm -rf /tmp/* && \
    texhash && \
    updmap-sys --quiet --enable Map mtpro2.map && \
    updmap-sys --quiet --disable Map=belleek.map
