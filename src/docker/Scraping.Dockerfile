# Copyleft (c) August, 2020, Oromion.

FROM python:3.12.5-alpine3.20

LABEL maintainer="Oromion <caznaranl@uni.pe>" \
	name="Web scraping" \
	description="Web scraping plus git." \
	url="https://gitlab.com/courses-2020-1/abet-project/container_registry" \
	vcs-url="https://gitlab.com/courses-2020-1/abet-project" \
	vendor="Oromion Aznarán" \
	version="1.0"

ENV MAIN_PKGS="\
	tzdata g++ gcc libxml2 libxml2-dev libxslt-dev git chromium" \
	TZ="America/Lima" \
	DEBIAN_FRONTEND=noninteractive \
	PIP_NO_CACHE_DIR=1

COPY requirements.txt .

RUN set -ex; \
    echo http://dl-cdn.alpinelinux.org/alpine/edge/main | tee /etc/apk/repositories; \
    echo http://dl-cdn.alpinelinux.org/alpine/edge/testing | tee -a /etc/apk/repositories; \
    echo http://dl-cdn.alpinelinux.org/alpine/edge/community | tee -a /etc/apk/repositories; \
    apk --no-cache upgrade && apk add --no-cache $MAIN_PKGS && \
	cp /usr/share/zoneinfo/America/Lima /etc/localtime && \
    apk del tzdata && \
	rm -rf /var/cache/apk/* && \
	pip install -r requirements.txt
