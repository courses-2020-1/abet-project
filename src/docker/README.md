## Build and push

```console
~/abet project » COMPOSE_DOCKER_CLI_BUILD=0 DOCKER_BUILDKIT=0 docker-compose build --pull
~/abet project » docker-compose push
```
