# Copyleft (c) August, 2020, Oromion.

FROM rocker/r-rmd:latest

LABEL maintainer="Oromion <caznaranl@uni.pe>" \
    name="EpiModel" \
    description="EpiModel." \
    url="https://gitlab.com/courses-2020-1/abet-project/container_registry" \
    vcs-url="https://gitlab.com/courses-2020-1/abet-project" \
    vendor="Oromion Aznarán" \
    version="1.0"

ENV MAIN_PKGS="\
    git" \
    DEBIAN_FRONTEND=noninteractive

RUN rm -f /etc/localtime && \
    ln -s /usr/share/zoneinfo/America/Lima /etc/localtime && \
    apt-get update -qq && \
    apt-get install -yq --no-install-recommends $MAIN_PKGS && \
    rm -rf /var/lib/apt/lists/*
