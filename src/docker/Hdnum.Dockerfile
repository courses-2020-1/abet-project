# Copyleft (c) August, 2020, Oromion.

FROM oblique/archlinux-yay

LABEL maintainer="Oromion <caznaranl@uni.pe>" \
    name="Dune Arch" \
    description="Dune in Arch." \
    url="https://gitlab.com/courses-2020-1/abet-project/container_registry" \
    vcs-url="https://gitlab.com/courses-2020-1/abet-project" \
    vendor="Oromion Aznarán" \
    version="1.0"

ENV MAIN_PKGS="\
    ghostscript clang gnuplot"

RUN sudo -u aur yay --noconfirm -Sy $MAIN_PKGS && \
    sudo -u aur yay --afterclean --removemake --save && \
    sudo -u aur yay -Qtdq | xargs -r sudo -u aur yay --noconfirm -Rcns && \
    rm -rf /home/aur/.cache
