# Copyleft (c) August, 2020, Oromion.
# Docs: https://www.digitalocean.com/community/tutorials/how-to-install-git-on-debian-10 https://github.com/Seanny123/dockerfiles/blob/master/pyjulia/Dockerfile
FROM julia:1.10.5-bookworm

LABEL maintainer="Oromion <caznaranl@uni.pe>" \
	name="DiffEq" \
	description="Julia image with DifferentialEquations packages." \
	url="https://gitlab.com/courses-2020-1/abet-project/container_registry" \
	vcs-url="https://gitlab.com/courses-2020-1/abet-project" \
	vendor="Oromion Aznarán" \
	version="1.0"

ARG MAIN_PKGS="\
	git \
	"

ARG GIT_BUILD_PKGS="\
	make \
	libssl-dev \
	libghc-zlib-dev \
	libcurl4-gnutls-dev \
	libexpat1-dev \
	gettext \
	unzip \
	"

ARG	GCC_PKGS="\
	gcc \
	g++ \
	python \
	"

ARG DEBIAN_FRONTEND=noninteractive

ARG JULIA_PROJECT=/root

COPY Manifest.toml $JULIA_PROJECT/Manifest.toml
COPY Project.toml $JULIA_PROJECT/Project.toml

ARG GIT_VERSION=2.46.0

RUN rm -f /etc/localtime && \
	ln -s /usr/share/zoneinfo/America/Lima /etc/localtime && \
	apt-get update -qq && \
	apt-get install -yq --no-install-recommends $MAIN_PKGS $GIT_BUILD_PKGS && \
	cd /tmp && curl -LO https://github.com/git/git/archive/v$GIT_VERSION.tar.gz && \
	unzip v$GIT_VERSION && cd git-$GIT_VERSION && make prefix=/usr/local all && \
	make prefix=/usr/local install && \
	apt-get install -yq --no-install-recommends $GCC_PKGS &&
# echo export PATH="/usr/local/bin:${PATH}" >> /etc/profile && \
# source /etc/profile && \
RUN	julia -e 'using Pkg;Pkg.instantiate();Pkg.add("PackageCompiler");using PackageCompiler;create_sysimage([:DifferentialEquations, :Plots, :Agents, :PyPlot, :StatsBase]; cpu_target="generic",replace_default=true);'

RUN	apt-get -yq purge $BUILD_PKGS && \
	apt-get -yq autoremove && \
	rm -rf /var/lib/apt/lists/*
