#!/usr/bin/env/python
# -*- coding: utf-8 -*-

from non_linear import times, yobs, y
from matplotlib.pyplot import legend, plot, savefig

plot(times[1::], yobs, marker='o', linestyle='none')
plot(times, y[:, 0], color='C0', alpha=0.5, label=f'$S\left(t\right)$')
plot(times, y[:, 1], color='C1', alpha=0.5, label=f'$I\left(t\right)$')
legend()
savefig("non_linear.svg")
