
#!/usr/bin/env/python
# -*- coding: utf-8 -*-

from scipy.integrate import odeint
from numpy import arange
from numpy.random import normal, seed
# For reproducibility
seed(20394)

freefall = lambda y, t, p: 2.0 * p[1] - p[0] * y[0]

# Times for observation
times = arange(0, 10, 0.5)
gamma, g, y0 = 0.4, 9.8, -2
y = odeint(freefall, t=times, y0=y0, args=tuple([[gamma, g]]))
yobs = normal(y, 2)
