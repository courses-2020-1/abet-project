#!/usr/bin/env/python
# -*- coding: utf-8 -*-
from freefall import times, y, yobs
from matplotlib.pyplot import legend, plot, style, xlabel, ylabel, savefig
style.use('seaborn-darkgrid')

plot(times, yobs, label='observed speed', linestyle='dashed', marker='o', color='red')
plot(times, y, label='True speed', color='k', alpha=0.5)
legend()
xlabel('Time (Seconds)')
ylabel(r'$y\left(t\right)$');
savefig("plot_freefall.svg")
