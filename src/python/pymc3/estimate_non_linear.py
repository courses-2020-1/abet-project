#!/usr/bin/env/python
# -*- coding: utf-8 -*-

from pymc3.ode import DifferentialEquation
from pymc3 import Model, HalfCauchy, Lognormal, Normal, sample, Bound, Deterministic
from non_linear import SIR, yobs
from math import log
from numpy import arange
from arviz import from_pymc3

sir_model = DifferentialEquation(
    func=SIR,
    times=arange(0.25, 5, 0.25),
    n_states=2,
    n_theta=2,
    t0=0,
)

with Model() as model4:
    sigma = HalfCauchy('sigma', 1, shape=2)

    # R0 is bounded below by 1 because we see an epidemic has occured
    R0 = Bound(Normal, lower=1)('R0', 2, 3)
    lam = Lognormal('lambda', log(2), 2)
    beta = Deterministic('beta', lam * R0)

    sir_curves = sir_model(y0=[0.99, 0.01], theta=[beta, lam])

    Y = Lognormal('Y', mu=log(sir_curves), sigma=sigma, observed=yobs)

    trace = sample(2000, tune=1000, target_accept=0.9, cores=1)
    data = from_pymc3(trace=trace)
