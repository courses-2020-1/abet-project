#!/usr/bin/env/python
# -*- coding: utf-8 -*-

from pymc3.ode import DifferentialEquation
from pymc3 import Model, HalfCauchy, Lognormal, Normal, sample, sample_prior_predictive, sample_posterior_predictive
from freefall import freefall, times, yobs
from arviz import from_pymc3, plot_posterior
from math import log
from matplotlib.pyplot import savefig

ode_model = DifferentialEquation(
    func=freefall,
    times=times,
    n_states=1,
    n_theta=2,
    t0=0
)

with Model() as model:
    # Specify prior distributions for soem of our model parameters
    sigma = HalfCauchy('sigma', 1)
    gamma = Lognormal('gamma', 0, 1)

    # If we know one of the parameter values, we can simply pass the value.
    ode_solution = ode_model(y0=[0], theta=[gamma, 9.8])
    # The ode_solution has a shape of (n_times, n_states)

    Y = Normal('Y', mu=ode_solution, sigma=sigma, observed=yobs)

    prior = sample_prior_predictive()
    trace = sample(2000, tune=1000, cores=1)
    posterior_predictive = sample_posterior_predictive(trace)

    data = from_pymc3(trace=trace, prior=prior, posterior_predictive=posterior_predictive)

plot_posterior(data)
savefig("estimation_model_1_freefall.svg")


with Model() as model2:
    sigma = HalfCauchy('sigma', 1)
    gamma = Lognormal('gamma', 0, 1)
    # A prior on the acceleration due to gravity
    g = Lognormal('g', log(10), 2)

    # Notice now I have passed g to the odeparams argument
    ode_solution = ode_model(y0=[0], theta=[gamma, g])

    Y = Normal('Y', mu=ode_solution, sigma=sigma, observed=yobs)

    prior = sample_prior_predictive()
    trace = sample(2000, tune=1000, target_accept=0.9, cores=1)
    posterior_predictive = sample_posterior_predictive(trace)

    data = from_pymc3(trace=trace, prior=prior, posterior_predictive=posterior_predictive)

plot_posterior(data)
savefig("estimation_model_2_freefall.svg")



with Model() as model3:
    sigma = HalfCauchy('sigma', 1)
    gamma = Lognormal('gamma', 0, 1)
    g = Lognormal('g', log(10), 2)
    # Initial condition prior.  We think it is at rest, but will allow for perturbations in initial velocity.
    y0 = Normal('y0', 0, 2)

    ode_solution = ode_model(y0=[y0], theta=[gamma, g])

    Y = Normal('Y', mu=ode_solution, sigma=sigma, observed=yobs)

    prior = sample_prior_predictive()
    trace = sample(2000, tune=1000, target_accept=0.9, cores=1)
    posterior_predictive = sample_posterior_predictive(trace)

    data = from_pymc3(trace=trace, prior=prior, posterior_predictive=posterior_predictive)

plot_posterior(data, figsize=(13,3))
savefig("estimation_model_3_freefall.svg")
