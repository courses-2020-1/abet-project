#!/usr/bin/env/python
# -*- coding: utf-8 -*-

if __name__ == "__main__":
    import plot_freefall
    import estimate_freefall
    import plot_non_linear
