#!/usr/bin/env/python
# -*- coding: utf-8 -*-

from scipy.integrate import odeint
from numpy import arange, log
from numpy.random import lognormal

def SIR(y, t, p):
    ds = -p[0] * y[0] * y[1]
    di = p[0] * y[0] * y[1] - p[1] * y[1]
    return [ds, di]

times = arange(0, 5, 0.25)
beta, gamma = 4, 1.0

# Create true curves
y = odeint(SIR, t=times, y0=[0.99, 0.01], args=((beta, gamma),), rtol=1e-8)
# Observational model.  Lognormal likelihood isn't appropriate, but we'll do it anyway
yobs = lognormal(mean=log(y[1::]), sigma=[0.2, 0.3])
