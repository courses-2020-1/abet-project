Python
------

.. code:: console

	~/abet project/src/python/scraping » poetry shell
	~/abet project/src/python/scraping » python main.py > ../../TeX/output.txt
