#!/usr/bin/env/python
# -*- coding: utf-8 -*-

from html_download import (
    daily,
    accumulated,
    death_accumulated)

if __name__ == "peru_stats":
    print("Confirmados_hoy", int(daily[0].text.strip().replace(" ", "")))
    print("Recuperados_hoy", int(daily[1].text.strip().replace(" ", "")))
    print("Fallecidos_hoy", int(daily[2].text.strip().replace(" ", "")))
    print("Confirmados", int(accumulated[0].text.strip().replace(" ", "")))
    print("Recuperados", int(accumulated[1].text.strip().replace(" ", "")))
    print("Fallecidos", int(death_accumulated[0].text.strip().replace(" ", "")))
