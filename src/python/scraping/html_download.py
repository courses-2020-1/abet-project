#!/usr/bin/env/python
# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
from requests import get


URL_TRUST_PERU = "https://www.gob.pe/coronavirus"

page = get(URL_TRUST_PERU)
soup = BeautifulSoup(page.content, "html.parser")

daily = soup.find_all(attrs={"class": "font-bold text-2xl md:pb-1"})
accumulated = soup.find_all(attrs={"class": "text-base w-full tracking-tight md:mt-2"})
death_accumulated = soup.find_all(attrs={"class": "text-base w-full mt-4 tracking-tight md:mt-2"})

from pyppeteer import launch
from requests_html import AsyncHTMLSession


URL_ORCEBOT = "https://covid19.orcebot.com"

from pathlib import Path
from platform import release

if release() == '5.10.6-arch1-1':
    chromium_path = str(Path.home()) + "/.local/share/pyppeteer/local-chromium/588429/chrome-linux/chrome"
else:
    chromium_path = "/usr/bin/chromium-browser"

# (chromium_path := chromium_path)
#(chromium_path := str(Path.home().chmod(0o700)) + '/.local/share/pyppeteer/')

async def get_orcebot():
    asession = AsyncHTMLSession()
    browser = await launch({'ignoreHTTPSErrors':True, 'headless':True, 'handleSIGINT':False, 'handleSIGTERM':False, 'handleSIGHUP':False, 'executablePath': chromium_path, 'args': ['--no-sandbox', '--disable-setuid-sandbox']})
    asession._browser = browser
    r = await asession.get(URL_ORCEBOT)
    await r.html.arender()
    return r


asession = AsyncHTMLSession()
result = asession.run(get_orcebot)
selector = "body > #app > div > main > div > div > div > div > div > div > div > #casosRegiones > div.v-data-table__wrapper > table > tbody > tr"

#def my_custom_decorator(funcion):
#    def wrapper():
#        pass
#    pass

#@my_custom_decorator
def finder(pattern):
    value = result[0].html.find(selector, containing=pattern)
    return value
