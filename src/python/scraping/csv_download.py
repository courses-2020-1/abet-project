#!/usr/bin/env/python
# -*- coding: utf-8 -*-

from csv import reader
from requests import Session


URL_DEATH_WORLD = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_global.csv"
URL_CONFIRMED_WORLD = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv"
URL_RECOVERED_WORLD = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_recovered_global.csv"

URL_WHO = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/who_covid_19_situation_reports/who_covid_19_sit_rep_time_series/who_covid_19_sit_rep_time_series.csv"

URL_DEATH_PERU = "https://cloud.minsa.gob.pe/s/Md37cjXmjT9qYSa/download"
URL_POSITIVE_PERU = "https://cloud.minsa.gob.pe/s/Y8w3wHsEdYQSZRp/download"

def parser_csv(URL_CSV, decoder='utf8'):
    """Get the csv file from link with header.

    Args:
        URL_CSV (string): Uniform Resource Locator (URL).
        decoder (str, optional): Reverse rules to convert csv. Defaults to 'utf8'.

    Returns:
        _csv.reader: An iterator, each iterations returns a row of the CSV file.
    """
    with Session() as s:
        download = s.get(URL_CSV)
        decoded_content = download.content.decode(decoder)
        table_world = reader(decoded_content.splitlines(), delimiter=',')
        next(table_world)
    return table_world
