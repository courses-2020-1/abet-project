#!/usr/bin/env/python
# -*- coding: utf-8 -*-

from csv_download import (
    parser_csv,
    URL_DEATH_WORLD,
    URL_CONFIRMED_WORLD,
    URL_RECOVERED_WORLD)


cummulative_death_world = [int(row[-1]) for row in parser_csv(URL_DEATH_WORLD)]
cummulative_confirmed_world = [int(row[-1]) for row in parser_csv(URL_CONFIRMED_WORLD)]
cummulative_recovered_world = [int(row[-1]) for row in parser_csv(URL_RECOVERED_WORLD)]

if __name__ == "covid19_world":
    print(f"WorldwideDeaths {sum(cummulative_death_world)}")
    print(f"WorldwideConfirmed {sum(cummulative_confirmed_world)}")
    print(f"WorldwideRecovered {sum(cummulative_recovered_world)}")
