#!/usr/bin/env/python
# -*- coding: utf-8 -*-

def euler(f, y0, a, b, h):
	t, y = a, y0
	while t <= b:
		t += h
		y += h * f(t,y)
	print(f"{t:6.3f} {y:6.3f}")


def newtoncooling(time, temp):
	return (1 + temp)**(1/temp)

if __name__ == "euler_method":
    euler(newtoncooling, 1, 0, 1, 1/100000)
