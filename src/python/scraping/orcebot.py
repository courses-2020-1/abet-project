#!/usr/bin/env/python
# -*- coding: utf-8 -*-

from html_download import finder


lima_stats = finder("Lima")[0].text.split()
callao_stats = finder("Callao")[0].text.split()

if __name__ == "orcebot":
    print(f"LimaConfirmed {lima_stats[1]}")
    print(f"LimaLast8Days {lima_stats[2][1:]}")
    print(f"LimaLetality {lima_stats[3]}")
    print(f"CallaoConfirmed {callao_stats[1]}")
    print(f"CallaoLast8Days {callao_stats[2][1:]}")
    print(f"CallaoLetality {callao_stats[3]}")
