#!/usr/bin/env/python
# -*- coding: utf-8 -*-
from sys import version_info
from datetime import datetime


now = datetime.now().strftime("%d/%m/%Y %H:%M:%S")

if __name__ == "version":
    print(f"{now}. Using the version of Python {'.'.join(map(str, version_info[:3]))}.")
