#!/usr/bin/env/python
# -*- coding: utf-8 -*-

from csv_download import (
    parser_csv,
    URL_DEATH_PERU,
    URL_POSITIVE_PERU)


row_death_peru_count = sum(1 for row in parser_csv(URL_DEATH_PERU, decoder = 'cp1252'))
row_positive_peru_count = sum(1 for row in parser_csv(URL_POSITIVE_PERU, decoder = 'cp1252'))

if __name__ == "covid19_peru":
    print(f"PeruviansDeaths {row_death_peru_count}")
    print(f"PeruviansConfirmed {row_positive_peru_count}")
