\section{Metodología}

La aparición del modelo de~\textcite{kermack1927} en 1927 fue uno de
los hitos más importante de la \emph{epidemiología matemática} del siglo
XX~\parencite{martcheva2015}, formularon este modelo para describir
la epidemia de peste que sufrió India en 1906.

Se trata del modelo SIR en el que el tamaño de la población se
mantiene constante.
La intención de los autores no eran la predicción exacta de los casos
fatales durante la epidemia, sino contestar la pregunta ¿qué factores
determinan tanto la magnitud como la terminación de la epidemia en la
población de Bombay y, en general, en cualquier población humana?

En su enfoque tenían como postulados básicos los siguientes:
\begin{itemize}
	\item La enfermedad que iban a estudiar debía ser viral o
	      bacteriana y ser transmitida por contacto directo de persona
	      a persona.
	\item Al inicio de la epidemia solamente una fracción de la
	      población era contagiosa.
	\item La población sería una población cerrada y a excepción de
	      las pocas personas inicialmente enfermas, todas las demás
	      serán suceptibles de enfermarse.
	\item El individuo sufre el curso completo de la enfermedad para al
	      final recuperarse adquiriendo inmunidad, o morir.
	\item La población total de las personas sería constante y sin
	      dinámica demográfica.
\end{itemize}

%Referencia del Libro: Modelos matemáticos en epidemiología: enfoques y enlaces (Jorge X. Velasco Hernández)

Asi mismo se consideran tres variables dependientes del
tiempo:

\begin{itemize}
	\item El número de individuos susceptibles: $S\left(t\right)$.
	\item El número de individuos infectados: $I\left(t\right)$.
	\item El número de individuos recuperados o muertos: $R\left(t\right)$.
\end{itemize}

Dado que se supone que la población se mantiene constante, entonces
$N=S\left(t\right)+I\left(t\right)+R\left(t\right)$ para cualquier
instante de tiempo $t$ y donde $N$ representa el número total de
individuos que forman dicha población.
Además se tienen en cuenta dos parámetros, a saber:


\begin{description}
	\item[El índice de transmisión $\beta$] es la tasa por unidad de
	      tiempo, de que el individuo susceptible se contagie al
	      estar en contacto con un individuo afectado.
	\item[El índice de recuperación o muerte $\alpha$] es la tasa por unidad de
	      tiempo, de que el individuo se recupere.
\end{description}

\subsection{Modelo SIR simple}

En este modelo a analizar la población será constante.
Consideraremos que no hay nacimientos ni muertes.

\subsubsection*{Formulación del modelo}

Los diversos estados del modelo serán:
\begin{equation}\Large
	\begin{tikzcd}
		S \arrow[r, "\beta SI"] & I \arrow[r, "\alpha I"] & R
	\end{tikzcd}
\end{equation}
Para formular el modelo epidemiológico utilizaremos las derivadas
para expresar cambios de los diversos estados del modelo:
\begin{align*}
	S^{\prime}
	           & =-\beta SI       \\
	I^{\prime}
	           & =\beta SI-\alpha I \\
	R^{\prime} & =\alpha I.
\end{align*}

Suponemos además que se verifican las condiciones iniciales:
\begin{align*}
	S\left(0\right)
	                & =S_{0}\geq0,          \\
	I\left(0\right)
	                & =I_{0}\geq0,          \\
	R\left(0\right) & =R_{0}\geq0.          \\
\end{align*}

\subsubsection*{Estudio analítico}

La derivada del estado suceptible es siempre no positivo $\left(S^{\prime}<0\right)$, por lo que el número de individuos susceptibles disminuirá mientras existan individuos infectados.
La derivada del estado resistente es siempre no negativa $\left(R>0\right)$, provocando que el número de recuperados aumente mientras existan individuos infecciosos.

Para saber si habrá un brote epidémico haremos el siguiente analisis:

\textbf{Caso I:} Si $I^{prime}>0$ en el tiempo $t_0$, entonces, $I^{\prime}(0)=I_0(\beta{S_0}-\alpha)>0$, si $S_0>\frac{\alpha}{\beta}$ $\left(R_0=S_0\frac{\beta}{\alpha}>1\right)$, por tanto el número de infecciosos aumentará y habrá una epidemia. Entonces para algun $t>0$, existirá un brote epidémico si $I(t)>I_0$.
\textbf{Caso II:} Si $I^{\prime}<0$ en el tiempo $t_0$, entonces, $I^{\prime}(0)=I_0(\beta{S_0}-\alpha)<0$, si $S_0<\frac{\alpha}{\beta}$ $\left(R_0=S_0\frac{\beta}{\alpha}<1\right)$, por tanto no habrá una epidemia, pues por lo mencionado, $S^{\prime}<0$ para cualquier instante de tiempo $t$, por tanto $S^{\prime}(t)\leq{S_0}$, para cualquier $t\geq0$. Si consideramos inicialmente la condición $S_0\leq\frac{\alpha}{\beta}$, siempre se cumplirá que $I^{\prime}(t)=I(\beta{S}-\alpha)\leq0$ entonces $I_0\geq{I(t)}$, para cualquier $t\geq0$.

%Gráfica1

De las ecuaciones de $S$ y de $R$ (con $\rho=\frac{\alpha}{\beta}$):
$$\frac{dS}{dR}=-\frac{S}{\rho}\Longrightarrow S=S_0e^{-R/\rho}$$

En la vida real puede ser dificil medir $I\left(t\right)$, y puede ser más fácil contar a los muertos:
\begin{equation}
\begin{split}
\frac{dR}{dt}&=\alpha I=\alpha(N-R-S)=\alpha(N-R-S_0e^{-R/\rho})\\
&\approx\alpha[N-S_0+(\frac{S_0}{\rho}-1)R-\frac{S_0R^2}{2\rho^2}]\nonumber
\end{split}
\end{equation}
Resolviendo la ecuación diferencial
$$R(t)=\frac{\rho^2}{S_0}[(\frac{S_0}{\rho}-1)+a\tanh(\frac{a\alpha t}{2}-\phi)]$$
$$a=[(\frac{S_0}{\rho}-1)^2+\frac{2S_0(N-S_0)}{\rho^2}]^{1/2},\quad\phi=\frac{\tanh^{-1}(\frac{S_0}{\rho}-1)}{a}$$
Derivando
$$\frac{dR}{dt}=\frac{\alpha a^2\rho^2}{2S_0}\cosh^{-2}(\frac{a\alpha t}{2}-\phi)$$
Comparando los datos reales de la plaga ocurrida en Bombay, Kermack y McKendrick obtuvieron
$$\frac{dR}{dt}=890\cosh^{-2}(0.2t-3.4)$$
La siguiente figura ilustra la situación y, al mismo tiempo, muestra cómo este modelo, a pesar de ser algo básico, se puede ajustar a la realidad bastante bien

%Gráfica2
%Referencia de libro:Mathematical Biology: I. An Introduction, Third Edition


\subsection{Modelo SIR con nacimiento y muerte natural}
Ahora consideraremos un caso más general debido a que tendremos en
cuenta las muertes y nacimientos debido a causas naturales.
Tomaremos la misma tasa de nacimientos que que de muertes, haciendo
el tamaño de la población constante.
Ademas, no consideraremos las muertes por enfermedad, por lo que el
modelo será util para analizar enfermedades con baja mortalidad.

Este modelo es un derivado de Kermack y McKendrick que se pueden
conconsiderar como ejemplos paradigmáticos y a partir de los cuales
se han desarrollado nuevos modelos matemáticos, son los debidos a Hethcote
y a Dieckmann y Heesterbeek.
El primero difiere el de Kermack y McKendrick en la incorporación de
la tasa de mortalidad.
\subsubsection*{Formulación del modelo}

Los diversos estados del modelo serán:
\begin{equation*}\Large
	\begin{tikzcd}
		\arrow[d, "\mu"] & & \\
		S \arrow[r, "\beta SI"] \arrow[d, "\mu S"]  & I \arrow[r, "\alpha I"] \arrow[d, "\mu I"] & R \arrow[d, "\mu R"]\\
		\phantom{A} & \phantom{A} & \phantom{A}
	\end{tikzcd}
\end{equation*}
En este modelo utilizaremos las ecuaciones:
\begin{align*}
	S^{\prime}
	 & =-\beta SI+\mu\left(N-S\right), \\
	I^{\prime}
	 & =\beta SI-\alpha I-\mu I,       \\
	R^{\prime}
	 & =\alpha I-\mu R,
\end{align*}

Esta vez incorporamos el parámetro $\mu>0$, que representa la tasa de
natalidad/mortalidad por causas naturales.
Generalmente consideramos que el tiempo de vida promedio se sitúa en
torno a los $75$ años, por lo que $\mu=\dfrac{1}{75\cdot365}$ en caso
de que tomemos los días como unidad de tiempo, lo que equivale a
suponer que cada año nace/muere una persona de cada $75$.
Como consideramos que la población tiene un tamaño constante, la tasa
de natalidad será la misma que la de mortalidad.

Suponemos además que se verifican las condiciones iniciales:

\begin{align*}
	S\left(0\right)
	 & =S_{0}\geq0,          \\
	I\left(0\right)
	 & =I_{0}\geq0,          \\
	R\left(0\right)
	 & =R_{0}\geq0,          \\
	\end{align*}
Para saber si habrá un brote epidémico haremos el siguiente analisis:

\textbf{Caso I:} Si $I^{\prime}>0$ en el instante de tiempo $t=0$, entonces $I^{\prime}(0)=(\beta{S_0}-\alpha-\beta)I_0>0$, si $S_0>\frac{\alpha+\mu}{\beta}$ puesto que $I_0>0$. De esto se obtiene que $\frac{\beta}{\alpha+\mu}S_0>1$, lo cual indica que habrá un brote epidémico.
\textbf{Caso II:} Si $I^{\prime}<0$ en el instante de tiempo $t=0$, entonces $I^{\prime}(0)=(\beta{S_0}-\alpha-\beta)I_0<0$, si $S_0<\frac{\alpha+\mu}{\beta}$ puesto que $I_0>0$. De esto se obtiene que $\frac{\beta}{\alpha+\mu}S_0<1$, lo cual indica que no habrá un brote epidémico.

\subsection{Modelo SIR con muerte por enfermedad}

Ahora desarrollaremos un modelo de SIR más elaborado, además de
considerar la muerte natural de la población, tambien consideraremos
la muerte por enfermedad que sufran los individuos infecciosos.
Gracias a ello, podremos analizar enfermedades con distintos índices
de mortalidad.
Seguimos considerando que el número de individuos que fallece es el
mismo que el que nace, por lo que el número de individuos de la
población permanece constante.

\subsubsection*{Formulación del modelo}

\begin{equation*}\Large
	\begin{tikzcd}
		\arrow[d, "\mu+\theta\alpha I"] & & \\
		S \arrow[r, "\beta SI"] \arrow[d, "\mu S"]
		& I \arrow[r, "\left(1-\theta\right)\alpha I"] \arrow{d}{\mu I}[swap]{\theta\alpha I}
		& R \arrow[d, "\mu R"]\\
		\phantom{A} & \phantom{A} & \phantom{A}
	\end{tikzcd}
\end{equation*}
En este modelo utilizaremos las ecuaciones:
\begin{align*}
	S^{\prime}
	           & =-\beta SI+\mu\left(1-S\right)+\theta\alpha I, \\
	I^{\prime} & =
	\beta SI-\mu I-\theta\alpha I-\left(1-\theta\right)\alpha I \\
	I^{\prime} & =\beta SI-\mu I-\alpha I,                      \\
	R^{\prime} & =\left(1-\theta\right)\alpha I-\mu R,
\end{align*}

En este modelo introducimos el parametro $\theta\in\left(0,1\right)$,
que mediante una simplificación representa la proporción de
individuos que abandonan el estado infeccioso que mueren por culpa de
la enfermedad.
Al considerar que la población tiene un tamaño constante, la tasa de
natalidad será la misma que la suma de las de mortalidad.
Suponemos además que se verifican las condiciones iniciales:
\begin{align*}
	S\left(0\right)
	 & =S_{0}\geq0,          \\
	I\left(0\right)
	 & =I_{0}\geq0,          \\
	R\left(0\right)
	 & =R_{0}\geq0,          \\
	S\left(t\right)+
	I\left(t\right)+
	R\left(t\right)
	 & =S_{0}+I_{0}+R_{0}=1.
\end{align*}
