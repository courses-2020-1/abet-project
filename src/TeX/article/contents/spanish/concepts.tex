\section{Conceptos previos} % Le corresponde a Óscar.

Se definirá algunos conceptos previos sobre ecuaciones diferenciales
ordinarias necesarias para el entendimiento de los métodos que
utilizaremos en este artículo.
\begin{definition}[Sistema de ecuaciones diferenciales ordinarias de primer orden]
	Es aquel sistema de que tiene la siguiente forma:
	\[
		\diff{x_{i}}{t}=
		f_{i}\left(t,x_{1},x_{2},\ldots,x_{n}\right),\quad
		i=1,2,\ldots,n
	\]
	donde
	\begin{itemize}
		\item $x_{1},x_{2},x_{3},\ldots,x_{n}$ son las variables de estado.
		\item $t$ es la variable independiente.
	\end{itemize}
\end{definition}
A dicho sistema podemos darle condiciones iniciales a las variables
de estado y de este modo obtenemos el \emph{problema de Cauchy}.
\begin{align*}
	\diff{x}{t}
	 & =f\left(t,x\right) \\
	x\left(t_{0}\right)
	 & =x_{0}
\end{align*}

\begin{definition}[Solución de un sistema de ecuaciones diferenciales]
	Es una función
	\begin{math}
		t\mapsto\phi\left(t\right)=
		\left(\phi_{1}\left(t\right),\phi_{2}\left(t\right),
		\phi_{3}\left(t\right),\ldots,\phi_{n}\left(t\right)\right)
	\end{math}
	definida en algún intervalo $I\subset\mathbb{R}$ con valores en
	$\mathbb{R}^n$ la cual satisface la ecuación diferencial original.
	\begin{multline*}
		\diff{\phi_{i}\left(t\right)}{t}=
		f_{i}
		\left(t,\phi_{1}\left(t\right),\phi_{2}\left(t\right),
		\phi_{3}\left(t\right),\ldots,\phi_{n}\left(t\right)\right),\\
		i=1,2,\ldots,n\text{ y }t\in I.
	\end{multline*}
\end{definition}

\subsection{Algoritmo de Euler}

Consideremos el problema de valor inicial
\begin{align*}
	y^{\prime}
	 & =f\left(x,y\right) \\
	y\left(x_{0}\right)
	 & =y_{0}.
\end{align*}
Para el desarrollo del algoritmo, asumiremos que la solución del
problema en el intervalo $\left(a,b\right)$ existe y es única.
Sea $h>0$ fijo y consideramos los puntos
\[
	x_{n}=x_{0}+nh,\quad n=0,1,2,\ldots.
\]
Nuestro objetivo con este método es encontrar una sucesión
$y_{0},y_{1},y_{2},\ldots$ que converja a la solución del problema
$\phi\left(x\right)$ de modo que
\[
	y_{n}\approx\phi\left(x_{n}\right)
\]
Ya que nuestra primera aproximación es exacta
$y_{0}=\phi\left(x_{0}\right)$, debemos encontrar una manera de
describir $y_{1},y_{2},\ldots$.
Para esto empezamos integrando nuestro problema inicial por ambos
lados desde $x_{n}$ a $x_{n+1}$, así obtenemos (sustituyendo
$\phi\left(x\right)$ por $y$)
\begin{align*}
	\phi\left(x_{n+1}\right)-\phi\left(x_{n}\right)
	 & =\int_{x_n}^{x_{n+1}}\phi^{\prime}\left(t\right)\dl{t}        \\
	 & =\int_{x_n}^{x_{n+1}}f\left(t,\phi\left(t\right)\right)\dl{t}
	\shortintertext{Resolviendo para $\phi\left(x_{n+1}\right)$ tenemos}
	\phi\left(x_{n+1}\right)
	 & =\phi\left(x_{n}\right)+
	\int_{x_n}^{x_{n+1}}f\left(t,\phi\left(t\right)\right)\dl{t}
	\shortintertext{Sin conocer $\phi\left(t\right)$, no podemos
	integrar $f\left(t,\phi\left(t\right)\right)dt$.
	Entonces, debemos aproximar la integral, la forma más fácil de
	aproximar el área bajo la curva $f\left(t,\phi(t)\right)$ como el
	rectángulo de base $\left[x_{n},x_{n+1}\right]$ y altura
	$f\left(x_{n},\phi\left(x_{n}\right)\right)$.
	Esto nos da}
	\phi\left(x_{n+1}\right)
	 & =\phi\left(x_{n}\right)+
	\left(x_{n+1}-x_{n}\right)
	f\left(x_{n},\phi\left(x_{n}\right)\right)
	\shortintertext{Sustituyendo $\left(x_{n+1}-x_{n}\right)$ por $h$ y
		la aproximación $y_{n}$ por $\phi\left(x_{n}\right)$ tenemos el
		siguiente esquema numérico}
	y_{n+1}
	 & =y_{n}+hf\left(x_{n},y_{n}\right)
\end{align*}

el cual es denominado \emph{método de Euler}.

% \begin{algorithm}%[ht!]
% 	\TitleofAlgo{Épsilon de la máquina}
% 	\For{k}{1}{100}
% 	\caption{Épsilon de la máquina}
% \end{algorithm}

\begin{algorithm}[ht!]
	\Datos{$f$, $y_{0}$, $a$, $b$, $h$}
	\Resultado{$\left(t_{i}, y_{i}\right)$ en el paso $i$.}
	$t\leftarrow  a$\;
	$y\leftarrow  y_{0}$\;
	\Mientras{$t\leq b$}{
		Escribir $\left(t, y\right)$\;
		$t\leftarrow  t + h$\;
		$y\leftarrow  y + h\cdot f\left(t, y\right)$\;
	}
	\caption{Método de Euler}
\end{algorithm}
Se quiere resolver $y^{\prime}\left(x\right)=f\left(x\right)$ en
$I=\left(a,b\right]$ con $y\left(a\right)=y_{a}$.
\begin{enumerate}[label={\bfseries Método~$\arabic*$},leftmargin=0em,itemindent=*]
	\item Para $N\in\mathbb{N}$ fijamos el tamaño del paso
	      $h=\dfrac{\left(b-a\right)}{N}$ y $x^{h}_{n}=a+nh$ con
	      $0\leq i\leq N$.

	      Para $h$ suficientemente pequeño,
	      \[
		      \frac{y\left(x^{h}_{n+1}\right)-
			      y\left(x^{h}_{n}\right)}{h}\approx
		      y^{\prime}\left(x^{h}_{n}\right)=
		      f\left(x^{h}_{n}\right)
	      \]
	      que conduce el esquema
	      \begin{math}
		      y^{h}_{n+1}=
		      y^{h}_{n}+
		      hf\left(x^{h}_{n}\right).
	      \end{math}

	\item Una buena aproximación a la integral es la regla
	      trapezoidal
	      \[
		      \int_{x}^{x+h}f\left(\xi\right)\dl{\xi}\approx
		      \frac{h}{2}\left(f\left(x\right)+
		      f\left(x+h\right)\right)
	      \]
	      que conduce al esquema
	      \[
		      y^{h}_{n+1}=
		      y^{h}_{n}+
		      \frac{h}{2}\left(f\left(x^{h}_{n}\right)+
		      f\left(x^{h}_{n+1}\right)\right).
	      \]
\end{enumerate}

Un sistema de ecuaciones diferenciales determina un vector solución
\begin{equation}
	u=
	\left(u_{1},\ldots,u_{d}\right)\colon\Omega^{d}\to\mathbb{R}^d.
\end{equation}
Para cada $u_{i}$, $i=1,\ldots,d$, la ecuación diferencial debe ser
satisfecha.
Dentro de estas ecuaciones diferenciales las variables solución
podrían depender de otra o no.
Ejemplos típicos de estos son los sistemas depredador-presa,
ecuaciones de Maxwell.
% TODO: Reference https://www.math.wisc.edu/~angenent/519.2016s/notes/picard.html
\begin{theorem}[Picard-Lindelöf]
	Sea $f\colon D\to\mathbb{R}^{d}$ continua y Lipschitz.
	Entonces existe para cada $\left(t_{0},y_{0}\right)\in D$ un
	$\varepsilon>0$ y una solución
	\begin{math}
		y\colon I\coloneqq
		\left[t_{0}-\varepsilon,
			t_{0}+\varepsilon\right]\to\mathbb{R}^{d}
	\end{math}
	del problema de valor inicial tal que
	\[
		y^{\prime}\left(t\right)=
		f\left(t,y\left(t\right)\right),\quad
		t\in I,\quad
		y\left(t_{0}\right)=y_{0}.
	\]
\end{theorem}

Consideremos el sistema de primer orden de EDOs en la forma explícita
\begin{multline}
	y^{\prime}\left(t\right)=
	f\left(t,y\left(t\right)\right),\quad
	t\in\left(t_{0},t_{0}+T\right],\\
	y\left(t_{0}\right)=u_{0}
\end{multline}
para determinar la solución desconocida
$u\colon\left[t_{0},t_{0}+T\right]\to\mathbb{R}^{d}$.
En componentes esto se lee
\[
	\begin{pmatrix}
		y^{\prime}_{1}\left(t\right) \\
		\vdots                       \\
		y^{\prime}_{d}\left(t\right) \\
	\end{pmatrix}=
	\begin{pmatrix}
		f_{1}\left(t,y_{1}\left(t\right),
		\ldots,y_{d}\left(t\right)\right) \\
		\vdots                            \\
		f_{d}\left(t,y_{1}\left(t\right),
		\ldots,y_{d}\left(t\right)\right) \\
	\end{pmatrix}
\]
El lado derecho
\begin{math}
	f\colon\left[t_{0},t_{0}+T\right]\times
	\mathbb{R}^{d}\to\mathbb{R}^{d}
\end{math}
es Lipschitz-continua
\[
	\left\|f\left(t,y\right)-f\left(t,w\right)\right\|\leq
	L\left(t\right)\left\|y-w\right\|.
\]
Así, el sistema tiene única solución.

\begin{theorem}[Teorema de Taylor con resto de Lagrange]
	Sea $u\colon I\to\mathbb{R}$ $\left(m+1\right)$-veces
	continuamente diferenciable.
	Entonces, para $t$, $t+\Delta t\in I$, se cumple
	\begin{multline*}
		y\left(t+\Delta t\right)=
		\sum_{k=0}^{m}\frac{y^{k}\left(t\right)}{k!}\Delta t^{k}+
		\frac{y^{\left(m+1\right)}
			\left(t+\theta\Delta t\right)}{\left(m+1\right)!}
		\Delta t^{m+1},\\
		\theta\in\left[0,1\right].
	\end{multline*}
\end{theorem}
Como consecuencia del teorema de Taylor tenemos para $m=1$:
\begin{multline*}
	y\left(t+\Delta t\right)=
	y\left(t\right)+
	y\left(t\right)^{\prime}\Delta t+
	\frac{y^{\prime\prime}\left(t+\xi\right)}{2}\Delta t^{2},\\
	0\leq\xi\leq\Delta t.
\end{multline*}
Tomado por componentes, esto también es válido para $y$ con valor
vectorial
%TODO: Máxima verosimultud?, número necesario de pruebas rápidas.
\subsection*{Explícito Euler}

\begin{itemize}
	\item Escoja $N$ pasos de tiempo
	      \begin{multline*}
		      t_{0}<t_{1}<t_{2}<\cdots<t_{N-1}<t_{N}=
		      t_{0}+T,\\
		      \Delta t_{n}=
		      t_{n+1}-t_{n}.
	      \end{multline*}
	\item $y^{\Delta t}_{n}$ denota la aproximación de
	      $y\left(t_{n}\right)$ calculado en el paso de tamaño
	      $\Delta t$.
	\item Tome Taylor, use la EDO y omita el término de error para
	      obtener la \emph{aproximación del explícito Euler}
	      \begin{equation*}
		      \begin{split}
			      y\left(t_{n+1}\right)
			      &=y\left(t_{n}\right)+
			      \Delta t_{n}y^{\prime}\left(t_{n}\right)+\\
			      &\phantom{=}\frac{y^{\prime\prime}\left(t_{n}+\xi_{n}\right)}{2}\Delta t^{2}_{n}\\
			      &=y\left(t_{n}\right)+
			      \Delta t_{n}f^{\prime}\left(t_{n},y\left(t_{n}\right)\right)+\\
			      &\phantom{=}\frac{y^{\prime\prime}\left(t_{n}+\xi_{n}\right)}{2}\Delta t^{2}_{n}\\
			      \implies y^{\Delta t}_{n+1}
			      &=y^{\Delta t}_{n}+\Delta t_{n}f\left(t_{n},y^{\Delta t}_{n}\right).
		      \end{split}
	      \end{equation*}
	\item Asumiendo $y^{\Delta t}_{n}=y\left(t_{n}\right)$ y restando obtenemos
	      \begin{equation*}
		      y\left(t_{n+1}\right)-
		      y^{\Delta t}_{n+1}=
		      \frac{y^{\prime\prime}\left(t_{n}+\xi_{n}\right)}{2}\Delta t^{2}_{n}.
	      \end{equation*}
\end{itemize}

\subsection*{Implícito Euler}

\begin{itemize}
	\item Usar el teorema de Taylor de manera ligeramente diferente da
	      \begin{equation*}
		      \begin{split}
			      y\left(t_{n}\right)
			      &=y\left(t_{n+1}-\Delta t_{n}\right)\\
			      &=y\left(t_{n+1}\right)-
			      \Delta t_{n}y^{\prime}\left(t_{n+1}\right)+\\
			      &\phantom{=}\Delta t^{2}_{n}\frac{y^{\prime\prime}\left(t_{n+1}-\xi_{n}\right)}{2}\\
			      &=y\left(t_{n+1}\right)-
			      \Delta t_{n}f\left(t_{n+1},y\left(t_{n+1}\right)\right)+\\
			      &\phantom{=}\Delta t^{2}_{n}\frac{y^{\prime\prime}\left(t_{n+1}-\xi_{n}\right)}{2}
		      \end{split}
	      \end{equation*}
	      \begin{multline*}
		      \iff y\left(t_{n+1}\right)-
		      \Delta t_{n}f\left(t_{n+1},y\left(t_{n+1}\right)\right)=
		      y\left(t_{n}\right)-\\
		      \Delta t^{2}_{n}\frac{y^{\prime\prime}\left(t_{n+1}-\xi_{n}\right)}{2}.
	      \end{multline*}
	\item El cual resulta la \emph{aproximación del implícito Euler}
	      \begin{equation*}
		      y^{\Delta t}_{n+1}-
		      \Delta t_{n}f\left(t_{n+1},y^{\Delta t}_{n+1}\right)=
		      y^{\Delta t}_{n}.
	      \end{equation*}
\end{itemize}
\subsection{Método de Runge-Kutta}
La expresión general de un método Runge-Kutta de R-evaluaciones es 
\begin{equation} \label{eq1}
 \begin{split}
y_{n+1} & = y_n + h \Phi (t_n, y_n ; h), \\
 \Phi(t,y;h) & = \displaystyle \sum_{r = 1}^{R}{c_rk_r}, \\
 k_1 & = f(t,y), \\
 k_r & = \displaystyle f\left( t+ha_r , y+ h\sum_{s=1}^{r-1}{b_{rs}k_s} \right), r= 2,3,\ldots, R, \\
 a_r & = \displaystyle \sum_{s=1}^{r-1}{b_{rs}}, r= 2,3,\ldots, R.
\end{split} 
\end{equation}
Un método de Runge-Kutta utiliza. R evaluaciones de la función $f$ que son las derivadas de la solución $y(t)$ en varios puntos y después en $\Phi$ hace una media ponderada, esto hace que necesariamente se cumple $\displaystyle \sum_{r=1}^{R}{c_r}=1.$
\subsubsection{Método de Runge-Kutta de orden 4}
El método de Runge-Kutta de orden 4 es el método más usado al momento de hallar una aproximación a la solución del PVI. A continuación procederemos a formularlo.\\
Primero dado el problema
\begin{equation} \label{eq1}
\begin{split}
y' & = f(x,y) \\
y(x_0) & = y_0
\end{split}
\end{equation}
\begin{enumerate}
\item Se calcula la pendiente en el punto $(x_n , y_n)$
\begin{center}
$K_1 = f(x_n,y_n)$
\end{center}
% Imagen 1
\item Se toma la recta tangente en $(x_n, y_n)$ 
\begin{center}
$r : y = y_n + K_1 (x-x_n)$
\end{center}
Y la evaluamos en $x= x_n+\frac{h}{2}$; sea
\begin{center}
$y_m^* = y_n + \frac{h}{2}K_1$
\end{center}
$y_m^*$ es una estimación del valor de la solución en $x=x_n+\frac{h}{2}$
%Imagen 2
\item Se evalúa $f$ en $(x_n + \frac{h}{2}, y_m^*)$:
\begin{center}
$K_2 = f(x_n + \frac{h}{2}, y_n + \frac{h}{2}K_1)$
\end{center}
\item Se revisa la estimación $y_m^*$ utilizando $K_2$:
\begin{center}
$y_m = y_n + \frac{h}{2}K_2$
\end{center}
es la ordenada de la recta que pasa por $(x_n,y_n)$ con pendiente $K_2$,
\begin{center}
$s: y = y_n + K_2(x-x_n)$
\end{center}
correspondiente a $x = x_n + \frac{h}{2}$
\item Se evalúa $f$ en $(x_n + \frac{h}{2},y_m)$:
\begin{center}
$K_3 = f(x_n + \frac{h}{2}, y_n + \frac{h}{2}K_2)$
\end{center}
%Imagen 3
\item Utilizando la recta que pasa por $(x_n,y_n)$ con pendiente $K_3$,
\begin{center}
$v : y = y_n + K_3(x-x_n)$
\end{center}
se hace una primera estimación del valor de la solución en $x_n+1$,
\begin{center}
$y_{n+1}^{*} = y_n + hK_3$
\end{center}
\item Se evalúa $f$ en $(x_{n+1},y_{n+1}^*)$
\begin{center}
$K_4 = f(x_n + h, y_n + hK_3)$
\end{center}
%Imagen 4
Sistematizando la información dada, tenemos
\begin{center}
$x_{n+1} = x_n + h$ ,     $y_{n+1} = y_n + \frac{h}{6}(K_1 + 2K_2 + 2K_3 + K_4)$
\end{center}
\begin{equation} \label{eq1}
\begin{split}
K_1 & = f(x_n,y_n)\\
K_2 & = f(x_n + \frac{h}{2}, y_n + \frac{h}{2}K_1) \\
K_3 & = f(x_n + \frac{h}{2}, y_n + \frac{h}{2}K_2) \\
K_4 & = f(x_n + h, y_n + hK_3)
\end{split}
\end{equation}
\end{enumerate}
